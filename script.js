/*
Instructions for s19 Activity:
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

*/

const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}.`)
const address = ["258 Washington Ave NW,", "California", 9011]
const [street, state, zone] = address
console.log(`I live at ${street} ${state} ${zone}`);
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	feet: 20,
	in:3,
}
console.log(`${animal.name} was a ${animal.type}. He weighed at ${animal.weight} kgs with a measurement of ${animal.feet} ft ${animal.in} in.`)

const numbers = [1,2,3,4,5]
numbers.forEach ((nums) => console.log(`${nums}`));
const initialValue = 0;
const reduceNumber = numbers.reduce(
  (previousValue, currentValue) => previousValue + currentValue,
  initialValue
);
console.log(reduceNumber);


class  Dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const dog1 = new Dog();
dog1.name = "Frankie";
dog1.age = 5;
dog1.breed = "Miniature Dachshund"
console.log(dog1);